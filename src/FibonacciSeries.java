import java.util.Scanner;

public class FibonacciSeries {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int number=scanner.nextInt();
        System.out.println(fibonacciSeries(number));
    }

    private static int fibonacciSeries(int number) {
        if (number <= 1)
            return number;

        return fibonacciSeries(number - 1) + fibonacciSeries(number - 2);
    }
}
